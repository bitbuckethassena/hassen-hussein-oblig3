#foreach($i in $args){
  #$dato=$(Get-Date -UFormat "%A %m/%d/%Y %R %Z")
  #$process=$(Get-Process | Where-Object{$_.Id -eq $i})
  #Write-Output "******** Minne info om prosess med PID $i *******" | Out-File $i-.meminfo
  #$vm=$($process.vm)
  #Write-Output "Total Bruk av Virtuelt Minne (Virtual Memory Size): $vm `n | Out-File $i-.meminfo
  #$ws=$($process.ws)
  #Write-Output "Storrelse på Working Set: $ws | Out-File $i-.meminfo
#}

#$dato=Get-Date

foreach($i in $args){
  $process=$(Get-Process | Where-Object{$_.Id -eq $i})
  $vm=$($process.vm)
  $ws=$($process.ws)
  Write-Output "*****Minne info om prosess med PID $i ***** `n Total bruk av virtuelt minne: $vm `n Storrelse paa Working Set: $ws" | Out-File ".\$i.meminfo"
}
