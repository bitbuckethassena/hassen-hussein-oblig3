# Obligatorisk oppgave 3 - PowerShell pipelines og scripting

Denne oppgaven best�r av de f�lgende laboppgavene fra kompendiet:

* 11.6.c (Prosesser og tr�der)
* 12.4.c (En prosess sin bruk av virtuelt og fysisk minne)
* 13.10.b (Internminne)
* 13.10.c (Informasjon om deler av filsystemet)

SE OPPGAVETEKST I KOMPENDIET. HUSK � REDIGER TEKSTEN NEDENFOR!

## Gruppemedlemmer

**TODO: Erstatt med navn p� gruppemedlemmene**

* Hassen Afzal
* Hussein Afzal
# NOTE
* Har sammarbeidet med Madhushan, Mohammed abdulle og Stian Pedersen p� denn oppgaven.
* Har f�tt feilmeldinger p� pipelines mtp internminne scriptet. Vet ikke hvordan dette skal l�ses.
* Ellers har vi brukt dokumentasjon p� powershell p� nett og f�tt hjelp av andre elever i klassen.

## Sjekkliste

* Har navnene p� gruppemedlemmene blitt skrevet inn over? 
* Har l�ringsassistenter og foreleser blitt lagt til med leserettigheter?
* Er issue-tracker aktivert?
* Er pipeline aktivert, og returnerer pipelinen "Successful"?
