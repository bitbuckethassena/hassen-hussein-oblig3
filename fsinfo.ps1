$path="$($args[0])"
$diskSize = ((Get-Location).Drive).Used + ((Get-Location).Drive).Free
$full=((Get-Location).Drive).Used/$diskSize*100
Write-Output "Partisjonen $path befinner seg paa er $full % full"
$filer=$(Get-ChildItem $path | Where-Object { ! $_.PSIsContainer } )   
$tellfiler=$filer.Count
Write-Output "Det finnes $tellfiler filer"
$sfil=$($files | Select-Object -First 1 | Format-Table Name -HideTableHeaders ) 
$sfil2=$($files | Select-Object -First 1 | Format-Table Length -HideTableHeaders )   
Write-Output "Storste filen er:" $sfil "som er paa storrelsen:" $sfil2 
$gjennomsnitt=$($filer | Measure-Object -Average Length | Format-Table Average -HideTableHeaders)     
Write-Output "Gjennomsnittlig filstorrelse er" $gjennomsnitt
