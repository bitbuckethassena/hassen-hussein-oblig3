function menu(){

Write-Output "
1 - Hvem er jeg og hva er navnet paa dette scriptet? 
2 - Hvor lenge er det siden siste boot?
3 - Hvor mange prosesser og traader finnes?
4 - Hvor mange context switch'er fant sted siste sekund?
5 - Hvor stor andel av CPU-tiden ble benyttet i kernelmode og i usermode siste sekund?
6 - Hvor mange interrupts fant sted siste sekund?
9 - Avslutt dette scriptet `n
Velg en funksjon: 
"
}

function en(){
Write-Output "Jeg er:" $(whoami)
Write-Output "`nNavnet paa dette scriptet er:" $MyInvocation.ScriptName 
}

function to(){
Get-WmiObject win32_operatingsystem | select csname, @{LABEL="LastBootUpTime" ;EXPRESSION={$_.ConverttoDateTime($_.lastbootuptime)}} | Format-Table LastBootUpTime

####Funnet på nett (https://devblogs.microsoft.com/scripting/powertip-get-the-last-boot-time-with-powershell/)   
}   

function tre(){
$allProcesses = Get-Process
Write-Output "Prosesser:" $allProcesses.Count

$allThreads = Get-WmiObject win32_thread 
Write-Output "Traader:" $allThreads.Count
}

function fire(){
$cs=$(Get-Counter -Counter "\System\Context Switches/sec").CounterSamples | Format-Table CookedValue -HideTableHeaders    
Write-Output "Context Switch siste sekund:" $cs
}

function fem(){
$um=$(Get-Counter -Counter "\Processor(_total)\% User Time" -SampleInterval 1 -MaxSamples 1).CounterSamples | Format-Table CookedValue -HideTableHeaders
$km=$(Get-Counter -Counter "\Processor(_total)\% Privileged Time" -SampleInterval 1 -MaxSamples 1).CounterSamples | Format-Table CookedValue -HideTableHeaders
Write-Output "CPU Tid benyttet i User Mode:" $um
Write-Output "CPU Tid benyttet i Kernel Mode:" $km 
}

function seks(){
$i=$(Get-Counter -Counter "\Processor(_total)\Interrupts/sec").CounterSamples | Format-Table CookedValue -HideTableHeaders   
Write-Output "Interrupts siste sekund:" $i
}

do {
menu
$cmd = Read-Host
switch($cmd) {
    1 { en }
    2 { to }
    3 { tre }
    4 { fire }
    5 { fem }
    6 { seks }
    9 { exit }
}
} while ($cmd -ne 9)
